class WheelController

	totalSteps: undefined
	currentStep: 0
	oldStep: undefined
	oldDataY: 0
	oldPercValue: 0
	scrollDirection: ""
	isAnimated: false
	isWheel: true
	phantomArea: undefined


	constructor:->

	build:(_options)=>
		@_target = _options.target
		@totalSteps = _options.views.length
		@steps = _options.views
		@isScroll = _options.scroll_available
		@phantomArea = _options.phantom_area

		do @setListeners

	setListeners:=>
		@main = document.querySelectorAll(@_target)[0]
		@main.addEventListener "mousewheel", @onMouseWheel, false
		@main.addEventListener "DOMMouseScroll", @onMouseWheel, false

		$(window).on 'change_step', @onChangeStep

		$(window).scroll @onScroll if @isScroll

	onChangeStep:(event, id)=>
		@currentStep = id
		do @goToStep

	onMouseWheel:(event)=>
		event.preventDefault() if @currentStep < @totalSteps-1
		return if @isAnimated
		@isWheel = true
		if event.pageY < ($("#"+@phantomArea).offset().top + 500)
			
			if /MSIE/i.test(navigator.userAgent)
				_delta = event.wheelDelta/100
			else
				_delta = if event.detail isnt 0 then event.detail else (event.deltaY/100)

			if (_delta) > 0
				@currentStep++ if @currentStep < @totalSteps
			else
				@currentStep-- if @currentStep > 0

			do @goToStep


	onScroll:(e)=>
		return if @isWheel or @isAnimated
		@scrollDirection = if @oldDataY < $(window).scrollTop() then "down" else "up"
		clearTimeout @verifyInteraction
		@verifyInteraction = setTimeout =>
			clearTimeout @verifyInteraction

			_percValue = parseInt(($(window).scrollTop() / $(@_target).height())*10)
			@currentStep = _percValue if _percValue < @totalSteps
			@currentStep = @totalSteps-1 if _percValue >= @totalSteps
			
			$(window).trigger 'change_step', @currentStep
			@oldPercValue = _percValue
		, 500
		@oldDataY = $(window).scrollTop()

	goToStep:=>
		# return if @phantomArea isnt undefined and @steps[@currentStep] is @phantomArea
		@isAnimated = true
		_step = @steps[@currentStep]
		if @currentStep isnt @oldStep and $('#'+_step).length > 0
			$("html, body").stop().animate {
				scrollTop: $('#'+_step).offset().top
			}, 1000, 'easeInOutQuad', =>
				setTimeout @resetFlags, 500

			@oldStep = @currentStep

	resetFlags:=>
		@isAnimated = false
		@isWheel = false
				

module.exports = WheelController