class MathUtils

	degToRad: (@deg) ->
		return @deg * (Math.PI / 180)

	radToDeg: (@rad) ->
		return @rad * (180 / Math.PI)

	getRandomInt:(@min, @max) ->
    	return Math.floor(Math.random() * (@max - @min + 1)) + @min;

    getRandomArbitrary:(@min, @max) ->
    	return Math.random() * (@max - @min) + @min;

module.exports = MathUtils