class ViewsController
	@getInstance:(options)->
		@instance ?= new @(options)

	_viewsDictionary: {}
	_viewData:null
	_oldViewData:null
	_viewContainer:null
	_viewDataConfig:null
	_oldViewDataConfig:null

	constructor: (options) ->
		_.extend @, Backbone.Events

	init:(@_viewsData, @_view) =>
		for v in @_viewsData
			@_viewsDictionary[v.id] = v
		@_viewContainer = @_view

		return

	go: (id, content)->
		return if !@_viewsDictionary[id]? || (@_viewData?.id == id && !content?.allowRepeatView)
		@_viewData?.view?.destroy()
		@_viewData = @_viewsDictionary[id]
		@_oldViewData = @_viewData 
		
		@_content = if content != undefined then content else _.findWhere(window.views.data, {id: id})
		@_viewData.view = new @_viewData.class({data:@_viewData, content: @_content})
		@_viewData.view.render()

		@trigger "created-view", {type: "created-view", currentTarget: @_viewData.view}

		@_viewDataConfig = @_viewData.view.options.content.config
		@manageViews()
		@_oldViewDataConfig = @_viewDataConfig
		
	manageViews:=>
		clearTimeout @animOutTime

		if @_oldViewDataConfig and @_oldViewDataConfig.out_time
			$(@_viewContainer).find(".template").addClass @_oldViewDataConfig.out_anim
			@animOutTime = setTimeout =>
				@insertView()
			, @_oldViewDataConfig.out_time * 1000
		else
			@insertView()

		if window.config.color_board
			$(window.config.color_board).removeClass @_oldViewDataConfig.bg_color if @_oldViewDataConfig
			$(window.config.color_board).addClass @_content.config.bg_color

	insertView:=>
		$(@_viewContainer).html(@_viewData.view.el)

	clearAll:->
		@_content = null
		clearTimeout @clearTime
		clearTimeout @animOutTime

module.exports = ViewsController