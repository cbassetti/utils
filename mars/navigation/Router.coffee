class Router extends Backbone.Router

	routes:
		'': 'index'

	current: null

	initialize: (options) ->
		@viewsController = options.viewsController

		for v in options.viewsData
			@add(v.id)

		Backbone.history.start()

	add: (route) ->
		route = ":#{route}"
		@routes[route] = "changeView"
		@route route, "changeView"

	index: ->
		viewDefault = @viewsController._viewsData[0].id
		@changeView(viewDefault)

	changeView: (id) ->
		console.log "changeView"
		for route of @routes
			route = route.substring 1, route.length
			if route == id
				# route = view.substring 1, view.length
				routeFound = route
				break

		if routeFound? && routeFound != @current
			@current = routeFound
			@viewsController.go(routeFound)
		else
			window.location.hash = "/#{@current}"


module.exports = Router