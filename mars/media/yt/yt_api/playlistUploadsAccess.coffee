# Define some variables used to remember state.
playlistId = undefined
nextPageToken = undefined
prevPageToken = undefined
# After the API loads, call a function to get the uploads playlist ID.

handleAPILoaded = ->
  requestUserUploadsPlaylistId()
  return

# Call the Data API to retrieve the playlist ID that uniquely identifies the
# list of videos uploaded to the currently authenticated user's channel.

requestUserUploadsPlaylistId = ->
  # See https://developers.google.com/youtube/v3/docs/channels/list
  request = gapi.client.youtube.channels.list(
    mine: true
    part: 'contentDetails')
  request.execute (response) ->
    playlistId = response.result.items[0].contentDetails.relatedPlaylists.uploads
    requestVideoPlaylist playlistId
    return
  return

# Retrieve the list of videos in the specified playlist.

requestVideoPlaylist = (playlistId, pageToken) ->
  $('#video-container').html ''
  requestOptions = 
    playlistId: playlistId
    part: 'snippet'
    maxResults: 10
  if pageToken
    requestOptions.pageToken = pageToken
  request = gapi.client.youtube.playlistItems.list(requestOptions)
  request.execute (response) ->
    # Only show pagination buttons if there is a pagination token for the
    # next or previous page of results.
    nextPageToken = response.result.nextPageToken
    nextVis = if nextPageToken then 'visible' else 'hidden'
    $('#next-button').css 'visibility', nextVis
    prevPageToken = response.result.prevPageToken
    prevVis = if prevPageToken then 'visible' else 'hidden'
    $('#prev-button').css 'visibility', prevVis
    playlistItems = response.result.items
    if playlistItems
      $.each playlistItems, (index, item) ->
        displayResult item.snippet
        return
    else
      $('#video-container').html 'Sorry you have no uploaded videos'
    return
  return

# Create a listing for a video.

displayResult = (videoSnippet) ->
  title = videoSnippet.title
  videoId = videoSnippet.resourceId.videoId
  $('#video-container').append '<p>' + title + ' - ' + videoId + '</p>'
  return

# Retrieve the next page of videos in the playlist.

nextPage = ->
  requestVideoPlaylist playlistId, nextPageToken
  return

# Retrieve the previous page of videos in the playlist.

previousPage = ->
  requestVideoPlaylist playlistId, prevPageToken
  return