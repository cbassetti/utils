class YtPLayer

# 
# Author: Caio Bassetti (caiera) - 2015
# Controlador de Videos YouTube Player via YT Api
# 
# Parametros:
# 	@container: tag que abrangerá o player (div, section, ...)
# 	@videoId: id do video inicial
# 

	constructor:(_options) ->
		@player = undefined
		@done = false
		@defaultOptions = 
			container: null
			videoId: null
			width: 1280
			height: 720
			controls: 1
			autoplay: 1
			showinfo: 1
			rel: 1
			wmode: "transparent"
			onPlayerReady: @onPlayerReady 
			onPlayerPlaybackQualityChange: @onPlayerPlaybackQualityChange
			onPlayerStateChange: @onPlayerStateChange
			onPlayerError: @onPlayerError

		@_options = $.extend {}, @defaultOptions, _options

		do @onYouTubeIframeAPIReady

	onYouTubeIframeAPIReady: =>
		@player = new (YT.Player)(@_options.container,
			videoId: @_options.videoId
			width: @_options.width
			height: @_options.height
			suggestedQuality: "highres"
			playerVars: { 
				autoplay: @_options.autoplay
				controls: @_options.controls
				showinfo: @_options.showinfo
				wmode: @_options.wmode
				rel: @_options.rel 
				# listType: 'playlist',
				# list: 'PLtEr1Cy3yRFi1fsCEImP8O5S4rpEzB00x'
			}
			events: {
				'onReady': @_options.onPlayerReady
				'onPlaybackQualityChange': @_options.onPlayerPlaybackQualityChange
				'onStateChange': @_options.onPlayerStateChange
				'onError': @_options.onPlayerError
			})
		return
	
	#EVENTS
	onPlayerReady: (event) =>
		event.target.playVideo()
		return

	onPlayerError: (event) =>
		return

	onPlayerStateChange: (event) =>
		if event.data == YT.PlayerState.PLAYING and !@done
			setTimeout @stopVideo, 6000
			@done = true
		return

	onPlayerPlaybackQualityChange: (event) =>
		return

	#GET AND SET
	setPlaybackQuality: (str)=>
		@player.setPlaybackQuality str
		return

	getPlaybackQuality: =>
		return @player.getPlaybackQuality()


	getPlaylist:=>
		return @player.getPlaylist()

	getPlaylistIndex:=>
		return @player.getPlaylistIndex()

	loadVideoById: (id, startSeconds, suggestedQuality) =>
		@player.loadVideoById
			videoId: id
			startSeconds: startSeconds || 0
			suggestedQuality: suggestedQuality || "highres"

	loadVideoByUrl: (url, startSeconds, suggestedQuality) =>
		@player.loadVideoByUrl	
			mediaContentUrl: url
			startSeconds: startSeconds || 0
			suggestedQuality: suggestedQuality || "highres"

	loadPlaylist:(playlist, index, startSeconds, suggestedQuality)=>
		@player.loadPlaylist
			playlist: playlist
			index: index
			startSeconds: startSeconds || 0
			suggestedQuality: suggestedQuality || "highres"

	seekTo:(second)=>
		@player.seekTo second, false
		return

	getCurrentTime:=>
		return @player.getCurrentTime()

	getDuration:=>
		return @player.getDuration()

	#CONTROLS
	nextVideo:=>
		@player.nextVideo()
		return

	previousVideo:=>
		@player.previousVideo()
		return

	playVideoAt:(index)=>
		@player.playVideoAt(index)
		return

	stop: =>
		@player.stopVideo()
		return

	play: =>
		@player.playVideo()
		return

	pause: =>
		@player.pauseVideo()
		return

	destroy: =>
		@player.destroy()
		return

	#SOUND
	mute:=>
		@player.mute()
		return

	isMuted:=>
		return @player.isMuted()

	setVolume:(val)=>
		@player.setVolume(val)
		return

	getVolume:=>
		return @player.getVolume()

module.exports = YtPLayer