class snd

	snd: null
	bar: null

	constructor:(id)->
		if id isnt ""
			@snd = document.getElementById id
		else
			@snd = document.createElement 'audio'
			document.body.appendChild @snd


	load:(file)=>
		if @snd.canPlayType 'audio/mpeg'
			@snd.setAttribute 'src', file 
			@setListeners()

	setListeners:=>
		@snd.addEventListener "loadstart", @loadStart
		@snd.addEventListener "durationchange", @durationChange
		@snd.addEventListener "loadedmetadata", @loadedMetadata
		@snd.addEventListener "loaded", @loaded
		@snd.addEventListener "progress", @progress
		@snd.addEventListener "canplay", @canPlay
		@snd.addEventListener "canplaythrough", @canPlayThrough
		@snd.addEventListener "timeupdate", @timeUpdate
		@snd.addEventListener "ended", @ended

	loadStart:(e)=>
		# console.log "snd loadStart"


	durationChange:(e)=>
		# console.log "snd durationChange"


	loadedMetadata:(e)=>
		# console.log "snd loadedMetadata"

	loaded:(e)=>
		# console.log "snd loaded"

	progress:(e)=>
		# console.log "snd progress"

	canPlay:(e)=>
		# console.log "snd canPlay"

	canPlayThrough:(e)=>
		# console.log "snd canPlayThrough"

	timeUpdate:(e)=>
		@bar.style.width = parseInt(((@snd.currentTime / @snd.duration) * 100), 10) + "%" if @bar

	ended:(e)=>
		# console.log "snd ended"

	play:=>
		@snd.play() if @snd
	
	pause:=>
		@snd.pause() if @snd
	
	playAtTime:(val)=>
		@snd.currentTime = val if @snd
		@play() if @snd

	setVolume:(val)=>
		@snd.volume = val if @snd

	getVolume:=>
		return @snd.volume()


module.exports = snd