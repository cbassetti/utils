EventBroadcaster = require '../event/EventBroadcaster'

class Loader

	@getInstance:()->
		@instance ?= new @()

	imageSequences:{}

	constructor: ()->
		_.extend @, Backbone.Events

	init: (@options)->
		@loadConfig()

	loadConfig:->
		@loaderConfig = new createjs.LoadQueue true
		@loaderConfig.loadManifest [
			{id:'config', src:@options.configURL, type:createjs.LoadQueue.JSON},
			{id:'views', src:@options.viewsURL, type:createjs.LoadQueue.JSON}
		]

		@loaderConfig.on "complete", (e) =>
			# console.log "Config complete: ", @loaderConfig.getResult ('config')
			window.config = @loaderConfig.getResult 'config'
			window.config.preload_assets = @parseSequenceAssets window.config.preload_assets
			window.config.assets = @parseSequenceAssets window.config.assets

			window.views = @loaderConfig.getResult 'views'
			
			@preloadAssets()
	

	preloadAssets:->
		@loaderPreAssets = new createjs.LoadQueue true
		@loaderPreAssets.loadManifest(window.config.preload_assets)
		@loaderPreAssets.on "complete", @onPreAssetsComplete

	onPreAssetsComplete: =>
		# console.log "preloadComplete"
		@trigger 'preloadComplete'
		@loadAssets()

	loadAssets:->
		@loaderAssets = new createjs.LoadQueue true
		@loaderAssets.on "progress", @onProgress
		@loaderAssets.on "complete", @onAssetsComplete
		@loaderAssets.on "error", @onAssetsError
		@loaderAssets.loadManifest(window.config.assets)

	onAssetsError: (data) =>
		console.log "#{data}"

	onAssetsComplete: =>
		# console.log "onAssetsComplete"
		if  window.config.media.length > 0
			@loadMedia()
		else
			@trigger 'complete'

	onProgress: =>
		@trigger 'onProgress', @loaderAssets.progress

	onMediaProgressUpdate: =>
		# console.log '@arrPerc', @arrPerc
		total = 0
		for percent in @arrPerc
			total += percent.perc
		EventBroadcaster.trigger 'onMediaProgress', total / @arrPerc.length

	get:(id)=>
		result = @loaderPreAssets.getResult(id)
		if result?
			return result

		result2 = @loaderAssets.getResult(id)
		result2

	parseSequenceAssets:(assets) =>
		newAssets = []
		for node in assets
			if node.type == "image_sequence"
				@imageSequences[node.id] = []

				iRangeStart = node.src.indexOf("[")+1
				iRangeEnd = node.src.indexOf("]")
				range = node.src.substring(iRangeStart, iRangeEnd)
				rangeStr = "[#{range}]"
				range = range.split("-")
				iImageStart = parseInt(range[0])
				iImageEnd = parseInt(range[1])

				for i in [iImageStart..iImageEnd]
					
					digit = if node.zeroPad? then @zeroPad(i, node.zeroPad) else i
					newSrc = node.src.replace(rangeStr, digit)

					imageId = "#{node.id}-#{i}"
					newNode = {"id":imageId, "src":newSrc, "type":"image"}
					newAssets.push newNode

					@imageSequences[node.id].push imageId

			else
				newAssets.push node

		newAssets



module.exports = Loader