class Utils

	canvas: undefined
	context: undefined

	constructor:(canvasId)->
		console.log "canvas/Utils: ", canvasId
		@canvas = document.getElementById(canvasId);
		@context = @canvas.getContext("2d");

	# Set border color
	borderColor: (color) =>
		@context.strokeStyle = color

	# Set line width
	lineWidth: (width) =>
		@context.lineWidth = width

	# Set line style: butt, round, or square.                
	lineStyle: (style) =>
		@context.lineCap = cap

	# Begin path
	begin: =>
		@context.beginPath()

	# Move to
	moveTo: (x, y) =>
		@context.moveTo x, y

	# Line to
	lineTo: (x, y) =>
		@context.lineTo x, y

	# Create a Quadratic curves
	quadraticCurveTo: (controlX, controlY, endingPointX, endingPointY) =>
		@context.quadraticCurveTo controlX, controlY, endingPointX, endingPointY

	# Create a Bezier curve
	bezierCurveTo: (cPointX1, cPointY1, cPointX2, cPointY2, endPointX, endPointY) =>
		@context.bezierCurveTo cPointX1, cPointY1, cPointX2, cPointY2, endPointX, endPointY

	# End path
	end: =>
		@context.stroke()

	# Draw circle
	circle: (x, y, radius, fill_style) =>
		startingAngle = 0 * Math.PI
		endingAngle = 2 * Math.PI
		@context.beginPath()
		@context.arc x, y, radius, startingAngle, endingAngle, false
		@context.fillStyle = fill_style
		@context.fill()
		@context.stroke()

	# Draw line
	line: (x1, y1, x2, y2, cap) =>
		@context.beginPath()
		@context.moveTo x1, y1
		@context.lineTo x2, y2
		@context.stroke()

	# Draw arc
	arc: (x, y, radius, startAngle, endAngle) =>
		startingAngle = startAngle * Math.PI
		endingAngle = endAngle * Math.PI
		@context.beginPath()
		@context.arc x, y, radius, startingAngle, endingAngle, false
		@context.stroke()

	# Draw curve
	curve: (x, y, controlX, controlY, endingPointX, endingPointY) =>
		@context.beginPath()
		@context.moveTo x, y
		@context.quadraticCurveTo controlX, controlY, endingPointX, endingPointY
		@context.stroke()

	# Draw bezier
	bezier: (x, y, cPointX1, cPointY1, cPointX2, cPointY2, endPointX, endPointY) =>
		@context.beginPath()
		@context.moveTo x, y
		@context.bezierCurveTo cPointX1, cPointY1, cPointX2, cPointY2, endPointX, endPointY
		@context.stroke()

	# Draw rectangle
	rectangle: (topLeftCornerX, topLeftCornerY, width, height, fill_style) =>
		@context.beginPath()
		@context.rect topLeftCornerX, topLeftCornerY, width, height
		@context.fillStyle = fill_style
		@context.fill()
		@context.stroke()

	default:=>	   
		# Set default text font
		@textFont "12pt Arial"
		# Set default text color
		@textColor "#999"
		# Set default color
		@borderColor "#fff"

	# Clear
	clear:=>
		@context.clearRect 0, 0, @canvas.width, @canvas.height
		@context.beginPath()

	# Set canvas background color
	background: (color) =>
		@canvas.style.background = color

	# Hide cursor
	noCursor: =>
		@canvas.style.cursor = "url('data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=='), auto"

	# Show default cursor  
	defaultCursor: =>
		@canvas.style.cursor = "default"

	# Show custom cursor
	customCursor: (filename) =>
		@canvas.style.cursor = "url('" + filename + "'),auto"

	# TYPOGRAPHY
	#	-----------------------------------------

	# Set text font	
	textFont: (font) =>
		@context.font = font

	# Set text color
	textColor: (color) =>
		@context.fillStyle = color

	# Draw text
	text: (str, x, y) =>
		@context.fillText str, x, y

	# SPRITES
	#	-----------------------------------------
	sprite: (imageObj, x, y, filename, width, height) =>
		imageObj.src = filename
		@context.drawImage imageObj, x, y, width, height

module.exports = Utils