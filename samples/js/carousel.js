(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var Carousel, buildCarousel, init;

Carousel = require('./wunderman/plugins/Carousel.coffee');

init = (function(_this) {
  return function() {
    console.log("XXX init");
    return buildCarousel();
  };
})(this);

buildCarousel = (function(_this) {
  return function() {
    var carousel2, carouselIdentidade;
    carouselIdentidade = new Carousel({
      target: '#carousel1',
      winW: $(window).width(),
      speed: 500,
      arrowL: ".arrows .left",
      arrowR: ".arrows .right"
    });
    return carousel2 = new Carousel({
      target: '#carousel2',
      winW: $(window).width(),
      speed: 500
    });
  };
})(this);

$(document).ready(init);



},{"./wunderman/plugins/Carousel.coffee":2}],2:[function(require,module,exports){
var Carousel,
  bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

Carousel = (function() {
  function Carousel(settings) {
    this.settings = settings;
    this.clearTimer = bind(this.clearTimer, this);
    this.resetTimer = bind(this.resetTimer, this);
    this.updateTimer = bind(this.updateTimer, this);
    this.updateBullets = bind(this.updateBullets, this);
    this.bulletsControl = bind(this.bulletsControl, this);
    this.updateArrows = bind(this.updateArrows, this);
    this.arrowsControl = bind(this.arrowsControl, this);
    this.buildArrows = bind(this.buildArrows, this);
    this.buildBullets = bind(this.buildBullets, this);
    this.scrollImages = bind(this.scrollImages, this);
    this.move = bind(this.move, this);
    this.next = bind(this.next, this);
    this.prev = bind(this.prev, this);
    this.swipeStatus = bind(this.swipeStatus, this);
    this.setSizes = bind(this.setSizes, this);
    this.onResize = bind(this.onResize, this);
    this.build = bind(this.build, this);
    this.obj = $(this.settings.target);
    this.settings.currentIndex = this.settings.initialIndex || 0;
    this.settings.len = this.obj.find(".carousel-list li").length;
    this.settings.itemsPerPage = 1;
    this.settings.isItens = false;
    this.build();
    this.setSizes();
  }

  Carousel.prototype.build = function() {
    var swipeOptions;
    if ((this.settings.len * this.obj.find(".carousel-list li").width()) > this.obj.width()) {
      swipeOptions = {
        triggerOnTouchEnd: true,
        allowPageScroll: 'vertical',
        threshold: 75,
        swipeStatus: this.swipeStatus
      };
      this.obj.swipe(swipeOptions);
      if (this.obj.attr("time-interval")) {
        this.settings.durationSlide = this.obj.attr("duration-slide");
        this.updateTimer();
      }
      if (this.obj.attr("bullets")) {
        this.buildBullets();
      }
      if (this.obj.attr("arrows")) {
        $(".arrows").css("display", "block");
        this.buildArrows();
      }
    }
    if (this.obj.attr("item-per-page") && this.obj.attr("item-per-page") > 1) {
      this.settings.isItens = true;
      this.settings.itemsPerPage = this.obj.attr("item-per-page");
    }
    $(window).resize(this.onResize);
    return this.setSizes();
  };

  Carousel.prototype.onResize = function() {
    this.setSizes();
    return this.move(this.settings.containerW);
  };

  Carousel.prototype.setSizes = function() {
    var item;
    if (this.settings.isItens) {
      item = this.obj.find(".carousel-list li");
      if (item.length > 0) {
        this.obj.find(".carousel-container").addClass("more-items");
        this.settings.containerW = this.obj.find(".carousel-container").width();
        this.settings.itemW = item.width() + (item.css("padding-left").substring(0, item.css("padding-left").length - 2) * 2);
        this.obj.find(".carousel-list").css("width", this.settings.itemW * this.settings.len);
        return this.obj.find(".carousel-list").css("height", item.height());
      }
    } else {
      this.settings.containerW = $(window).width();
      this.obj.find(".carousel-list").css("width", this.settings.containerW * this.settings.len);
      return this.obj.find(".carousel-list li").css("width", this.settings.containerW);
    }
  };

  Carousel.prototype.swipeStatus = function(event, phase, direction, distance) {
    var duration;
    if (event.type !== "mouseup" || event.type !== "mousedown") {
      if (phase === 'move' && (direction === 'left' || direction === 'right')) {
        this.clearTimer();
        duration = 0;
        if (direction === 'left') {
          return this.scrollImages((this.settings.containerW * this.settings.currentIndex) + distance, duration);
        } else if (direction === 'right') {
          return this.scrollImages((this.settings.containerW * this.settings.currentIndex) - distance, duration);
        }
      } else if (phase === 'cancel') {
        return this.scrollImages(this.settings.containerW * this.settings.currentIndex, this.settings.speed);
      } else if (phase === 'end') {
        if (this.obj.attr("time-interval")) {
          this.resetTimer();
        }
        if (direction === 'right') {
          return this.prev();
        } else if (direction === 'left') {
          return this.next();
        }
      }
    }
  };

  Carousel.prototype.prev = function() {
    this.settings.currentIndex = Math.max(this.settings.currentIndex - 1, 0);
    if (this.settings.isItens) {
      return this.move(this.settings.itemW * this.settings.itemsPerPage);
    } else {
      return this.move(this.settings.containerW);
    }
  };

  Carousel.prototype.next = function() {
    if (this.settings.isItens) {
      this.settings.currentIndex = Math.min(this.settings.currentIndex + 1, this.settings.len - this.settings.itemsPerPage);
      return this.move(this.settings.itemW * this.settings.itemsPerPage);
    } else {
      this.settings.currentIndex = Math.min(this.settings.currentIndex + 1, this.settings.len - 1);
      return this.move(this.settings.containerW);
    }
  };

  Carousel.prototype.move = function(diffW) {
    return this.scrollImages(diffW * this.settings.currentIndex, this.settings.speed);
  };

  Carousel.prototype.scrollImages = function(distance, duration) {
    var value;
    this.obj.find(".carousel-list").css('transition-duration', (duration / 1000).toFixed(1) + 's');
    value = (distance < 0 ? '' : '-') + Math.abs(distance).toString();
    this.obj.find(".carousel-list").css({
      '-moz-transform': "translate(" + value + "px,0)",
      '-ms-transform': "translate(" + value + "px,0)",
      '-webkit-transform': "translate(" + value + "px,0)",
      'transform': "translate(" + value + "px,0)"
    });
    if (window.navigator.userAgent === "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Win64; x64; Trident/4.0; .NET CLR 2.0.50727; SLCC2; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; InfoPath.3)") {
      this.obj.find(".carousel-list").animate({
        "margin-left": value + "px"
      }, duration);
    }
    this.updateArrows();
    return this.updateBullets();
  };

  Carousel.prototype.buildBullets = function() {
    var className, i, j, ref;
    this.obj.append("<ul class='bullets'></ul>");
    for (i = j = 0, ref = this.settings.len - 1; 0 <= ref ? j <= ref : j >= ref; i = 0 <= ref ? ++j : --j) {
      if (i === 0) {
        className = "bullet selected";
      } else {
        className = "bullet";
      }
      this.obj.find(".bullets").append("<li id='bl-" + i + "' class='" + className + "'></li>");
    }
    return this.obj.find(".bullet").bind("click", this.bulletsControl);
  };

  Carousel.prototype.buildArrows = function() {
    this.arrowL = $(this.settings.arrowL);
    this.arrowR = $(this.settings.arrowR);
    this.arrowL.bind("click", this.arrowsControl);
    return this.arrowR.bind("click", this.arrowsControl);
  };

  Carousel.prototype.arrowsControl = function(e) {
    console.log("arrowsControl");
    e.preventDefault();
    if ($(e.currentTarget).hasClass("right")) {
      this.next();
    } else {
      this.prev();
    }
    this.updateArrows();
    if (this.obj.attr("time-interval")) {
      return this.resetTimer();
    }
  };

  Carousel.prototype.updateArrows = function() {
    var targetArrow;
    if (this.obj.attr("arrows")) {
      this.arrowL.css("opacity", 1);
      this.arrowR.css("opacity", 1);
      targetArrow = null;
      if (this.settings.currentIndex === this.settings.len - 1 || this.settings.currentIndex === this.settings.len - this.settings.itemsPerPage) {
        targetArrow = this.arrowR;
      } else if (this.settings.currentIndex === 0) {
        targetArrow = this.arrowL;
      }
      if (targetArrow !== null) {
        return targetArrow.css("opacity", 0.2);
      }
    }
  };

  Carousel.prototype.bulletsControl = function(e) {
    var bt, currentId;
    bt = e.currentTarget;
    currentId = $(bt).attr("id").substring(3, 4);
    if (this.settings.currentIndex !== $(bt).attr("id").substring(3, 4)) {
      this.scrollImages(this.settings.containerW * $(bt).attr("id").substring(3, 4), this.settings.speed);
      this.settings.currentIndex = currentId;
      this.updateBullets();
    }
    if (this.obj.attr("time-interval")) {
      return this.resetTimer();
    }
  };

  Carousel.prototype.updateBullets = function() {
    this.obj.find(".bullet").removeClass("selected");
    return this.obj.find("#bl-" + this.settings.currentIndex).addClass("selected");
  };

  Carousel.prototype.updateTimer = function() {
    return this.settings.updateCount = window.setInterval((function(_this) {
      return function() {
        if (_this.settings.currentIndex === _this.settings.len - 1) {
          _this.settings.currentIndex = -1;
        }
        return _this.next();
      };
    })(this), this.settings.durationSlide * 1000);
  };

  Carousel.prototype.resetTimer = function() {
    this.clearTimer();
    return this.updateTimer();
  };

  Carousel.prototype.clearTimer = function() {
    return window.clearInterval(this.settings.updateCount);
  };

  return Carousel;

})();

module.exports = Carousel;



},{}]},{},[1]);
